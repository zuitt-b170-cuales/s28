//1 Returns All

fetch("https://jsonplaceholder.typicode.com/todos").then((response) => response.json()).then((json) => console.log(json));

// 2 Returns Title Only
fetch("https://jsonplaceholder.typicode.com/todos?title").then((response) => response.json()).then((json) => console.log(json));

// 3 Returns only 1 item
fetch("https://jsonplaceholder.typicode.com/todos/1").then((response) => response.json()).then((json) => console.log(json[0]));

// 4 Print Status
fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => console.log(response.status));

// 5 Post Method
fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		userId: 1,
		title: "New Todo"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// 6 Update
fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Put Todo",
		description: "Put Description",
		status: "Put Status",
		datecompleted: "Today",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// 7
fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		userId: 1,
		title: "Updated Todo"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// 8
fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		userId: 1,
		status: "complete"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// 9
fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "DELETE"
})

